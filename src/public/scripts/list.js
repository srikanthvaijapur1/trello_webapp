const createList = (name, id) => {
    const listboard = document.querySelector(".list-board");
    const list = document.createElement("div");
    list.classList.add("listorder");
    list.id = id;
    list.innerHTML = `
    <div class="h6btn"><h5>${name}</h5><button type="reset" class="btn btn-light deletbutton"><img type="reset" src="./icons/delete.svg"></button></div>
                       </div><input placeholder="Add a card title" class ="cardInput" id="text${id}"></input>
                       <button type="button" class="btn btn-primary cardbutton"> Add a Card
                       </button>`;
    let createList1 = document.querySelector(".createList");
    listboard.insertBefore(list, createList1);
};

export default createList;