const key = 'ef571fe5c6cf0bd5501071ae167315e6';
const token = '6b56596cde0e7b105f19e1200897c651fa5f29eb26dd459ad03a61326c54befe';
const id = '60e5340f0de322656f95a5b9'

import data from "./data.js";


//create a Board
const createBoard = (boardname, id) => {
    console.log(id)
    const newLink = document.createElement("a");
    newLink.setAttribute("id", "board-link")
    newLink.setAttribute("href", `board.html?id=${id}&name=${boardname}`);
    const newBoard = document.createElement("div");
    newBoard.setAttribute("id", "board")
    const spanboard = document.createElement("span");

    spanboard.innerText = boardname

    newLink.appendChild(newBoard);
    newBoard.appendChild(spanboard);
    const boardbody = document.querySelector(".board-body");
    console.log(boardbody)
    // boardbody.append(newLink);

    const btnid = document.getElementById("btnid")

    boardbody.insertBefore(newLink, btnid)
}

(async () => {
    const data1 = await data();
    console.log(data)
    for (let board of data1) {
        // console.log(board.name, board.id)
        createBoard(board.name, board.id)
    }
})()

const addBoard = () => {
    return fetch('https://api.trello.com/1/boards/?key=' + key + '&token=' + token + '&name=' + document.getElementById("boardsname").value, {
            method: 'POST'
        })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.text();
        })
        .then(data => createBoard(data.name, data.id))
        .then(text => console.log(text))
        .catch(err => console.error(err));
}



document.getElementById("createboard").addEventListener("click", addBoard)