const createmodal = (id) => {
    const create = document.getElementById("createmodal")
    const div = document.createElement("div")
    div.innerHTML = `<div class="modal fade " id="modalview" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content modalclick" id=${id}>
            <div class="modal-header">
                <img src="./icons/window.svg" alt="">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-9">
                        <h4><img src="./icons/description.svg" alt="">Description</h4>
                        <div class="descriptiondiv"><textarea name="" id="textId" cols="50"
                                rows="4"></textarea><button class="btn btn-primary" id="textbutton">Save</button>
                        </div>
                        <button class="btn btn-default" id="descriptionbtn" style="width: 80%;height:3vw">Add more
                        details</button>  
                        <div class="checkbox">
                            <form action="check">
                                <input type="checkbox" class="form-check-input">
                                <p class="pcheck"></p>
                            </form>
                            <button class="btn btn-light">Add a item</button>
                        </div>
                    </div>
                    <div class="col-md-3 ml-auto">
                        <H5>ADD TO CART</H5>
                        <li><button class="btn btn-light colbtn" data-toggle="modal" data-target="#myModal"><img
                                    src="./icons/check.svg" alt="">Checklist</button></li>
                        <li><button class="btn btn-light colbtn"><img src="./icons/member.svg"
                                    alt="">Member</button></li>
                        <li><button class="btn btn-light colbtn"><img src="./icons/label.svg" alt="">Label</button>
                        </li>
                        <li><button class="btn btn-light colbtn"><img src="./icons/attachment.svg"
                                    alt="">Attachment</button></li>
                        </li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`
    create.appendChild(div)

}

export default createmodal;