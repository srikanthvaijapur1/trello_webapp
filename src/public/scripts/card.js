const createCard = (name, id, listId) => {
    const list = document.getElementById(listId);
    const card = document.createElement("div");
    card.classList.add("cardName");
    card.setAttribute("data-toggle","modal")
    card.setAttribute("data-target","#modalview")
    card.id = id;
    card.innerHTML = `<p class="pcard">${name}</p>`;
    const cardInput = list.querySelector(".cardInput");
    list.insertBefore(card, cardInput);
};

export default createCard;