import createList from "./list.js";
import createCard from "./card.js";
import description from "./description.js";
import checklistItem from "./checklist.js";
import createmodal from "./modal.js";

const key = 'ef571fe5c6cf0bd5501071ae167315e6';
const token = '6b56596cde0e7b105f19e1200897c651fa5f29eb26dd459ad03a61326c54befe';
const id = '60e5340f0de322656f95a5b9';

const urlPath = window.location.href;
const newUrlPath = new URL(urlPath);
const boardId = newUrlPath.searchParams.get("id");
const boardName = newUrlPath.searchParams.get("name");
const boardTitle = document.getElementById("projectName");
const modalbox = document.getElementById("myModal");
const listBoard = document.querySelector(".list-board");

/* getting board name and id */

boardTitle.innerText = boardName;


window.addEventListener("load", async function () {
    const modalboxid = await document.getElementById("modalview");

    const promise = await fetch(
        `https://api.trello.com/1/boards/${boardId}/lists?key=${key}&token=${token}`, {
            method: "GET"
        }
    );

    // Get Lists on a Board

    const listArrData = await promise.json();
    listArrData.forEach(async function (listObj) {
        createList(listObj.name, listObj.id);
        const promise = await fetch(
            `https://api.trello.com/1/lists/${listObj.id}/cards?key=${key}&token=${token}`, {
                method: "GET"
            }
        );
        // Get Cards in a List
        const cardArrData = await promise.json();
        await cardArrData.forEach(async function (cardObj) {
            createCard(cardObj.name, cardObj.id, listObj.id);
            createmodal(cardObj.id)
        });

    });

});




//Creating a list in a board

document.querySelector(".listbutton").addEventListener("click", () => {
    return fetch('https://api.trello.com/1/lists?key=' + key + '&token=' + token + '&name=' + document.getElementById("inputList").value + '&idBoard=' + boardId, {
            method: 'POST'
        })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
        })
        .then(data => {
            document.getElementById("inputList").value = '';
            createList(data.name, data.id)
        }).catch(err => console.error(err));

    modalbox.addEventListener("click", (e) => {
        if (e.target.type == "button") {
            const text = document.getElementById("inputcheck").value;

            fetch('https://api.trello.com/1/cards/' + id + '/checklists?key=' + key + '&token=' + token + '&name=' + text, {
                    method: 'POST'
                })
                .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    );
                    return response.json();
                })
                .then(data => console.log(data))
                .catch(err => console.error(err));
        }
    })
})



//lists on a board


listBoard.addEventListener("click", (e) => {

    //creating a card in a list

    if (e.target.type == "button") {

        const listId = e.target.parentElement.id;

        fetch('https://api.trello.com/1/cards?key=' + key + '&token=' + token + '&idList=' + listId + '&name=' +
                document.querySelector(`#text${listId}`).value, {
                    method: 'POST'
                })
            .then(response => response.json())
            .then(data => {
                document.querySelector(`#text${listId}`).value = '';
                return createCard(data.name, data.id, data.idList);
            }).catch(err => console.error(err));
    }

    //Deleting a list in a board
    else if (e.target.type == "reset") {

        const listId = e.target.parentElement.parentElement.id;

        fetch(`https://api.trello.com/1/lists/${listId}?key=${key}&token=${token}&closed=true`, {
                method: 'PUT'
            })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            }).catch(err => console.error(err));
    }

    //CardName for Modal
    else if (e.target.className == "pcard") {
        document.getElementsByClassName("modal-title")[0].innerText = e.target.innerText
    }

    //Adding  and posting description
    document.getElementById("textbutton").addEventListener("click", () => {
        const text = document.querySelector("#textId").value
        const cardId = e.target.parentElement.id;
        console.log("text", text);
        fetch(`https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}&desc=${text}`, {
                method: 'PUT'
            }).then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            }).then(data => {
                console.log(data)
                description(data.id, data.desc)
            })
            .catch(err => console.error(err));
        document.querySelector("#textId").value = '';
    })

    const id = e.target.parentElement.id
    modalbox.addEventListener("click", (e) => {
        if (e.target.type == "button") {
            createmodal(id)
            const text = document.getElementById("inputcheck").value;

            fetch('https://api.trello.com/1/cards/' + id + '/checklists?key=' + key + '&token=' + token + '&name=' + text, {
                    method: 'POST'
                })
                .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    );
                    return response.json();
                })
                .then(data => checklistItem = (data.id, data.name))
                .catch(err => console.error(err));
        }
    })
})




// posting modalboxdata